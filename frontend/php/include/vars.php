<?php
# This file is part of the Savane project
# <http://gna.org/projects/savane/>
#
# $Id: vars.php 6431 2006-11-22 10:14:55Z yeupou $
#
#  Copyright 1999-2000 (c) The SourceForge Crew
#
#  Copyright 2003-2004 (c) Mathieu Roy <yeupou--gnu.org>
# 
# The Savane project is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# The Savane project is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with the Savane project; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


$LICENSE = array();
$LICENSE_URL = array();
$DEVEL_STATUS = array();

# sys_incdir is normally safe (see pre.php)
require($GLOBALS['sys_incdir'].'/hashes.txt');

$SHELLS = array();
$SHELLS[1] = '/bin/bash';
$SHELLS[2] = '/bin/sh';
$SHELLS[3] = '/bin/ksh';

?>
