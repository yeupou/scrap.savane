<?php
# This file is part of the Savane project
# <http://gna.org/projects/savane/>
#
# $Id: editgroupinfo.php 6101 2006-11-01 09:44:02Z yeupou $
#
#  Copyright 1999-2000 (c) The SourceForge Crew
#  Copyright 2000-2003 (c) Free Software Foundation
#                          Mathieu Roy <yeupou--gnu.org>
#
#  Copyright 2004-2006 (c) Mathieu Roy <yeupou--gnu.org>
#
# The Savane project is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# The Savane project is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with the Savane project; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


require "../../include/pre.php";

register_globals_off();
$group_id = sane_all("group_id");

require $GLOBALS['sys_www_topdir']."/include/vars.php";

session_require(array('group'=>$group_id,'admin_flags'=>'A'));

if (sane_post("update"))
{
  group_add_history ('Changed Public Info','',$group_id);

  $sql = 'UPDATE groups SET '
    ."group_name='".htmlspecialchars(sane_post("form_group_name"))."',"
    ."short_description='".htmlspecialchars(sane_post("form_shortdesc"))."',"
    ."long_description='".sane_post("form_longdesc")."',"
    
    ."devel_status='".sane_post("form_devel_status")."' "
    ."WHERE group_id=$group_id";

  $result = db_query($sql);
  if (!$result)
    { fb(_("Update failed."), 1); }
}

# update info for page
$res_grp = db_query("SELECT * FROM groups WHERE group_id=$group_id");
if (db_numrows($res_grp) < 1)
{
  exit_no_group();
}
$row_grp = db_fetch_array($res_grp);


site_project_header(array('title'=>_("Editing Public Info"),'group'=>$group_id,'context'=>'ahome'));



# ####################################### General Description

print form_header($PHP_SELF)
     .form_input("hidden", "group_id", $group_id);

print '
<p><span class="preinput">'._("Group Name:").'</span>
<br />&nbsp;&nbsp;&nbsp;'.form_input("text", 
				     "form_group_name", 
				     $row_grp['group_name'],
				     'size="60" maxlen="254"').'</p>';

print '
<p><span class="preinput">'.sprintf(_("Short Description %s:"), markup_info("none", ", 255 Characters Max")).'</span>
<br />&nbsp;&nbsp;&nbsp;'.form_textarea("form_shortdesc",
					$row_grp['short_description'],
					'cols="70" rows="3" wrap="virtual"').'</p>';

print '
<p><span class="preinput">'.sprintf(_("Long Description %s:"), markup_info("full")).'</span>
<br />&nbsp;&nbsp;&nbsp;'.form_textarea("form_longdesc",
					$row_grp['long_description'],
					'cols="70" rows="10" wrap="virtual"').'</p>';

$type_id = $row_grp['type'];
$result1 = db_query("SELECT * FROM group_type WHERE type_id='$type_id'");
$row_grp1 = db_fetch_array($result1);

if($DEVEL_STATUS1 = $row_grp1['devel_status_array']){
  $DEVEL_STATUS=preg_split("/\n/",$DEVEL_STATUS1);
}

if ($project->CanUse("devel_status"))
{
  print '
<p><span class="preinput">'
    ._("Development Status:").'</span><br />&nbsp;&nbsp;&nbsp;<select name="form_devel_status">';
  while (list($k,$v) = each($DEVEL_STATUS))
    {
      print '<option value="'.$k.'"';
      if ($k == $row_grp['devel_status'])
	{ print ' SELECTED'; }
      print '>'.$v;
      print '</option>';
    }
  print '</select></p>';
}

print form_footer();

site_project_footer(array());

?>
