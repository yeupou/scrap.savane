<?php
require_once 'PHPUnit/Framework.php';
require_once '../../frontend/php/include/utils.php';
require_once '../../frontend/php/include/user.php';
require_once '../../frontend/php/include/markup.php';

class MarkupTest extends PHPUnit_Framework_TestCase
  {
    public function testMarkupUnderscoreToEmphasis()
      {
        $data = array(
           ''
        => '',
           'No markup'
        => 'No markup',
           'No markup_for_this'
        => 'No markup_for_this',
           'Markup _for_ this'
        => 'Markup <em>for</em> this',
           '_Markup_ this'
        => '<em>Markup</em> this',
           '<p>_Markup_ this</p>'
        => '<p><em>Markup</em> this</p>',
           'Dont_Markup_ this'
        => 'Dont_Markup_ this',
           'Markup with _dot_.'
        => 'Markup with <em>dot</em>.',
           'Markup with _comma_, and _dot_.'
        => 'Markup with <em>comma</em>, and <em>dot</em>.',
           'Markup _several words as unit_'
        => 'Markup <em>several words as unit</em>',
           # This has been reported as bug #10571 on gna.org
           'standard logical operators (_not_, _and_ etc.)'
        => 'standard logical operators (<em>not</em>, <em>and</em> etc.)',
        );
        
        foreach ($data as $testdata => $expected)
          {
            $this->assertEquals($expected, _markup_inline($testdata));
	  }
      }
  }
?>
